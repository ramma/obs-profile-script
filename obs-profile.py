#!/usr/bin/python

# Allows for saving of multiple profiles.
# syntax - ./obs-profile --save <profile-name> --load <profile-name>

import sys
import getopt
import shutil
import os

def main(argv):
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hs:l:",["save=","load="])
        profiles = os.path.expanduser('~/.obs-studio/basic/profiles/')
        basic = os.path.expanduser('~/.obs-studio/basic/basic.ini')
        scenes = os.path.expanduser('~/.obs-studio/basic/scenes.json')
        service = os.path.expanduser('~/.obs-studio/basic/service.json')

    except getopt.GetoptError:
        print 'obs-profile.py --save <profile-name> --load <profile-name>'

    for opt, arg in opts:
        if opt == '-h':
            print 'obs-profile.py -save <profile-name> -load <profile-name>'

        elif opt in ("-s", "--save"):
            savename = os.path.expanduser('~/.obs-studio/basic/profiles/{}/'.format(arg))
            try:
                os.stat(savename)
            except:
                os.mkdir(savename)
            shutil.copy(basic, savename)
            shutil.copy(scenes, savename)
            shutil.copy(service, savename)
            print 'Saved as profile', arg

        elif opt in ("-l", "--load"):
            save_basic = os.path.expanduser('~/.obs-studio/basic/profiles/{}/basic.ini'.format(arg))
            save_scenes = os.path.expanduser('~/.obs-studio/basic/profiles/{}/scenes.json'.format(arg))
            save_service = os.path.expanduser('~/.obs-studio/basic/profiles/{}/service.json'.format(arg))
            shutil.copy(save_basic, basic)
            shutil.copy(save_scenes, scenes)
            shutil.copy(save_service, service)
            print 'Loaded from profile', arg

if __name__ == "__main__":
    main(sys.argv[1:])
