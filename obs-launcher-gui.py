#!/usr/bin/python

import sys
import getopt
import shutil
import os
from gi.repository import Gtk

profiles = os.path.expanduser('~/.obs-studio/basic/profiles/')
basic = os.path.expanduser('~/.obs-studio/basic/basic.ini')
scenes = os.path.expanduser('~/.obs-studio/basic/scenes.json')
service = os.path.expanduser('~/.obs-studio/basic/service.json')

if not os.path.exists(profiles):
    os.makedirs(profiles)

class MyWindow(Gtk.Window):
    
    def __init__(self):
        Gtk.Window.__init__(self, title="OBS-studio Profile Launcher")

        self.set_border_width(6)
        self.set_default_size(300, 150)

        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(self.vbox)

        self.dirname = Gtk.Entry() # Text box for save directory name.
#        self.dirname.set_text("Insert save name here")
        label = Gtk.Label("Profile save name") # Label things?
        self.vbox.pack_start(label, True, True, 0)
        self.vbox.pack_start(self.dirname, True, True, 0)

        self.box = Gtk.Box(spacing=6) # Box 1
        self.vbox.pack_start(self.box, True, True, 0)

        self.box2 = Gtk.Box(spacing=6) # Box 2
        self.vbox.pack_start(self.box2, True, True, 0)

        self.save = Gtk.Button(label="Save profile") # Save button.
        self.save.connect("clicked", self.on_save_clicked)

        self.load = Gtk.Button(label="Load profile") # Load button.
        self.load.connect("clicked", self.on_load_clicked)

        self.launch = Gtk.Button(label="Launch OBS") # Launch OBS with no options
        self.launch.connect("clicked", self.on_launch_clicked)
        
        self.launchopts = Gtk.Button(label="Launch OBS with options") # Launch OBS with options
        self.launchopts.connect("clicked", self.on_launchopts_clicked)

        # Box 1
        self.box.pack_start(self.save, True, True, 0)
        self.box.pack_start(self.load, True, True, 0)

        # Box 2
	self.box2.pack_start(self.launchopts, True, True, 0)
        self.box2.pack_start(self.launch, True, True, 0)

	# OBS launch options in prefix position
        label = Gtk.Label("Launch prefix")
        self.vbox.pack_start(label, True, True, 0)
        self.prefixopts = Gtk.Entry() # Text box for obs-studio launch options. 
        self.prefixopts.set_text("taskset -c 4,5,6,7")
        self.vbox.pack_start(self.prefixopts, True, True, 0)

	# OBS launch options in suffix position
        label = Gtk.Label("Launch suffix")
        self.vbox.pack_start(label, True, True, 0)
        self.suffixopts = Gtk.Entry() # Text box for obs-studio launch options. 
#        self.suffixopts.set_text("taskset -c 4,5,6,7")
        self.vbox.pack_start(self.suffixopts, True, True, 0)

    def on_save_clicked(self, widget): # Save button behaviour.
        savename = self.dirname.get_text()
        savename = os.path.expanduser('~/.obs-studio/basic/profiles/{}/'.format(savename))
        try:
            shutil.copy(basic, savename)
            shutil.copy(scenes, savename)
            shutil.copy(service, savename)

            dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO, # Saved dialog.
                    Gtk.ButtonsType.OK, "Saved as profile {}".format(self.dirname.get_text()))
            dialog.run()
            dialog.destroy()

        except IOError:
            os.mkdir(savename)
            self.on_save_clicked(widget)

    def on_load_clicked(self, widget): # Load button behaviour.
        savename = self.dirname.get_text()
        save_basic = os.path.expanduser('~/.obs-studio/basic/profiles/{}/basic.ini'.format(savename))
        save_scenes = os.path.expanduser('~/.obs-studio/basic/profiles/{}/scenes.json'.format(savename))
        save_service = os.path.expanduser('~/.obs-studio/basic/profiles/{}/service.json'.format(savename))
        shutil.copy(save_basic, basic)
        shutil.copy(save_scenes, scenes)
        shutil.copy(save_service, service)

        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO, # Saved dialog.                                  
                Gtk.ButtonsType.OK, "Loaded from profile {}".format(self.dirname.get_text()))
        dialog.run()
        dialog.destroy()


    def on_launchopts_clicked(self, widget): # Launch button behaviour.
        print('Launching obs with options {}'.format(self.prefixopts.get_text()))
        prefix = self.prefixopts.get_text()
	suffix = self.suffixopts.get_text()
        os.system('{} obs {}'.format(prefix,suffix))

    def on_launch_clicked(self, widget): # Launch button behaviour.
        print('Launching obs with no options.')
        os.system('obs')


win = MyWindow()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
